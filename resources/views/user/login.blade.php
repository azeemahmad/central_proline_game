<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proline</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet prefetch" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/app.css">
    <style>
        .help-block{
            color: red !important;
        }
    </style>

    <style type="text/css">
        tr,td,th {
            border: 1px solid lightgrey !important;
        }
        .popup-tnc {
            height: 80vh;
            overflow: hidden;
        }
        .popup-tnc .content{
            max-height: 100%!important;
            overflow: auto;
        }
        .popup-tnc .tnclist li{
            font-size: 17px;;
        }
        a.tc-title {
            /*color: #fff;
            background: #f42f24;
            font-size: 17px;
            padding: 5px;
            font-weight: 700;
            text-decoration: none;*/
                color: #fff;
                background: #f42f24;
                font-size: 17px;
                padding: 9px 25px;
                font-weight: 700;
                text-decoration: none;
                margin-top: 50px;
                border-radius: 4px;
        }
    </style>
</head>
<body class="bodyouter game-bglogin">

    <header class="logo-header">
        <img src="img/logo.png" class="aligh-left" alt="logo"> <img src="img/logo-lifestyle.png" class="aligh-right" alt="logo">
    </header>

<div class="container form-outer">
    <!-- <header>
        <img src="img/logo.png">
    </header> -->

    <!-- </div>

    <div class="container form-outer"> -->
    
    <form class="forminner" method="post" action="{{url('/login')}}">
        @csrf
        <div id="dispalyerrormessgae"></div>
        <!-- <h2>Contact Us</h2> -->
        <h3><strong>Scan play and win:</strong></h3>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <!-- <label for="first">First Name</label> -->
                    {{--<input type="text" class="form-control" placeholder="First Name" id="name" name="name">--}}
                    {{--@if ($errors->has('name'))--}}
                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                    {{--</span>--}}
                    {{--@endif--}}
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <!-- <label for="company">Company</label> -->
                    <input type="number" class="form-control" placeholder="Mobile No." id="mobile" name="mobile">
                    @if ($errors->has('mobile'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                    @endif
                </div>


            </div>
        </div>
        <!--  row   -->


        {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}

                {{--<div class="form-group">--}}
                    {{--<!-- <label for="email">Email address</label> -->--}}
                    {{--<input type="email" name="email" class="form-control" id="email" placeholder="Email ID">--}}
                    {{--@if ($errors->has('email'))--}}
                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        <!--  row   -->


        <!-- <label for="contact-preference">When is the best time of day to reach you?</label>


        <label for="newsletter">Would you like to recieve our email newsletter?</label> -->
        <!-- <div class="checkbox">

          <label>
            <input type="checkbox" value="Sure!" id="newsletter"> Sure!
          </label>
        </div> -->


        <button type="submit" class="btn btn-primary">SUBMIT</button>
    </form>

    <a href="{{url('/termandconditions')}}" class="tc-title"><strong class="tc-title">Terms & Conditions</strong></a>
</div>



{{--<a class="button tncbtn" href="{{url('/termandconditions')}}">Terms & Conditions</a>--}}
<div id="popup2" class="overlay">
    <div class="popup">
        <div class="popup-tnc">
            <h2>Terms & Conditions</h2>
            <a class="close" href="#">&times;</a>

            <div class="content">
                <ul class="tnclist">
                    <li>1. Voucher is valid at our Exclusive Proline Stores and <a href="https://www.prolineindia.com">Prolineindia.com</a></li>
                    <li>2. Vouchers are based on your time taken to finish the game.</li>
                    <li>3. Vouchers codes are sent to you via SMS/Email.</li>
                    <li>4. This voucher cannot be clubbed with any other offer.</li>
                    <li>5. Only one voucher can be redeemed at a time.</li>
                    <li> 6. For any questions please call our customer care number for details.</li>
                    {{--<li>1. You will receive a voucher based on your performance in your first game only. </li>--}}
                    {{--<li>2. 30 seconds and below - Voucher worth Rs. 1000 redeemable on shopping worth Rs. 1999.</li>--}}
                    {{--<li>3.  60 seconds  to 30 seconds - Vouchers worth 500 redeemable on shopping worth 1499.</li>--}}
                    {{--<li>4. 60 seconds and above - Vouchers worth Rs.250 redeemable on shopping worth 999.</li>--}}
                    {{--<li>5. Offer valid from 31st August to 15th September, 2019. </li>--}}
                    {{--<li>6. Offer Valid only at the Proline Store - Sarath City mall Hyd.   Gachibowli - Miyapur Road, White Field Rd, Kondapur, Hyderabad, Telangana 500084. </li>--}}
                </ul>

                <table style="width: 550px;" border="0" cellspacing="0" cellpadding="0">
                    <colgroup>
                        <col width="201"/>
                        <col width="229"/>
                        <col width="116"/>
                        <col width="98"/>
                    </colgroup>
                    <tbody>
                    <tr style="height: 20px;">
                        <td><strong>Time to complete</strong></td>
                        <td><strong>Offer</strong></td>
                        <td><strong>Validity</strong></td>
                    </tr>
                    <tr style="height: 20px;">
                        <td>Above 45 secs to 60 secs</td>
                        <td>above 999</td>
                        <td>22nd Sep 2019</td>
                    </tr>
                    <tr style="height: 20px;">
                        <td>Between 30 secs to 45 sec</td>
                        <td>Rs. 250 off on shopping above 1499</td>
                        <td>22nd Sep 2019</td>
                    </tr>
                    <tr style="height: 20px;">
                        <td>Under 30 Seconds&nbsp;</td>
                        <td>Rs. 500 off on shopping above 1999</td>
                        <td>22nd Sep 2019</td>
                    </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="js/app.js"></script>

{{--<script type="text/javascript">--}}
    {{--$(document).ready(function () {--}}

        {{--$('#registerationsform').submit(function(event){--}}
            {{--var name = $("#name").val();--}}
            {{--var email = $("#email").val();--}}
            {{--var mobile = $("#mobile").val();--}}
            {{--if(name=='' || email=='' || mobile==''){--}}
                {{--alert('All field compulsory');--}}
                {{--return false;--}}
            {{--}--}}
            {{--else if(mobile.length!=10){--}}
                {{--alert('Enter only 10 digit mobile number');--}}
                {{--return false;--}}
            {{--}--}}
            {{--else{--}}
                {{--event.preventDefault();--}}
                {{--formData = $( this ).serialize();--}}
                {{--$.ajax({--}}
                    {{--url: "/userregisterartion",--}}
                    {{--type: "post",--}}
                    {{--data: formData ,--}}
                    {{--success: function (response) {--}}
                        {{--if(response==1){--}}
                            {{--$('#dispalyerrormessgae').html('Your query submitted successfully');--}}
                            {{--$('#dispalyerrormessgae').css('color','green');--}}
                            {{--$("#registerationsform").trigger("reset");--}}
                            {{--window.location.href = '/home';--}}
                        {{--}--}}
                        {{--else if(response==3){--}}
                            {{--$('#dispalyerrormessgae').html('This email is already registered with us');--}}
                            {{--$('#dispalyerrormessgae').css('color','green');--}}
                        {{--}--}}
                        {{--else{--}}
                            {{--$('#dispalyerrormessgae').html('Your query not submitted successfully');--}}
                            {{--$('#dispalyerrormessgae').css('color','red');--}}
                            {{--$("#registerationsform").trigger("reset");--}}

                        {{--}--}}

                    {{--},--}}
                    {{--error: function() {--}}
                        {{--$('#dispalyerrormessgae').html('<h3>Your query not submitted successfully</h3>');--}}
                        {{--$('#dispalyerrormessgae').css('color','red');--}}
                        {{--$("#registerationsform").trigger("reset");--}}
                    {{--}--}}


                {{--});--}}

            {{--}--}}
        {{--});--}}


    {{--});--}}
{{--</script>--}}
</body>
</html>
