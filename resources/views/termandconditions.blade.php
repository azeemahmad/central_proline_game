<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proline</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet prefetch" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <link rel="stylesheet" href="css/app.css">

    <style type="text/css">
        tr, td, th {
            border: 1px solid lightgrey !important;
        }
        .popup{
            margin-top: 0px;
        }

        .popup-tnc {
            height: auto;
            overflow: hidden;
        }

        .popup-tnc .content {
            max-height: 100% !important;
            overflow: auto;
        }

        .popup-tnc .tnclist li {
            font-size: 20px;;
        }

        a.tc-title {
            color: #fff;
            background: #f42f24;
            font-size: 20px;
            padding: 5px;
            font-weight: 700;
            text-decoration: none;
        }
        .logo-header {
            display: flow-root;
        }
        .popup-tnc.container {
            display: grid;
            padding: 0px 5px;
        }
        ul.tnclist {
            width: 87%;
        }

        @media(max-width: 600px){
                .logo-header{
                display: flow-root;
                width: 93%;
                margin: 0;
                    padding-left: 5px;

            }
        .locator{
            display: contents;
        }
        }
    </style>
</head>
<body>

    <header class="logo-header">
        <img src="img/logo.png" class="aligh-left" alt="logo"> <img src="img/logo-lifestyle.png" class="aligh-right" alt="logo">
    </header>

<div class="container">
    <!-- <header>
        <a href="{{url('/')}}"><img src="img/logo.png"></a>
    </header> -->
    <div class="row"><!--popup-->
        <div class="container popup-tnc"><!--popup-tnc-->
            <h2>Terms & Conditions</h2>
            <div class="content">
                <ul class="tnclist">
                    <li>1. Voucher is valid only at Central Store in India.</li>
                    <li>2. Time to finish determines the value of the voucher.</li>
                    <li>3. This voucher cannot be clubbed with any other offer from Proline.</li>
                    <li>4. Vouchers codes are sent to you via SMS.</li>
                    <li>5. Only one voucher can be used by a customer.</li>
                    <li>6. Please contact customer care for any additional information needed.</li>
                </ul>
                <table border="0" width="95%" cellspacing="0" cellpadding="0"><colgroup><col width="201" /><col width="229" /><col width="116" /><col width="98" /></colgroup>
                    <tbody>
                    <tr>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl65" width="201" height="20">Complete the game in:</td>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl65" width="229">Offer&nbsp;</td>
                        <!-- <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl65" width="116">Valid from&nbsp;</td> -->
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl65" width="98">Valid to&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66" height="20">Under 15 Seconds  </td>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">Rs. 500 off on shopping  for Proline Apparel above 1999</td>
                        <!-- <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">23rd Sep 2019</td> -->
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">31st Oct 2019    &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66" height="20">Between 16 secs to 30 sec</td>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">Rs. 250 off on shopping  for Proline Apparel above 1499 &nbsp;</td>
                        <!-- <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">23rd Sep 2019&nbsp;&nbsp;</td> -->
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">31st Oct 2019 &nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66" height="20"> Above 31 secs to 60 secs  &nbsp;</td>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">Rs. 100 off on shopping for Proline Apparel above 999  &nbsp;</td>
                        <!-- <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">23rd Sep 2019&nbsp;&nbsp;</td> -->
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">31st Oct 2019 &nbsp;&nbsp;&nbsp;</td>
                    </tr>
                    </tbody>
                </table>


            </div>
            <div><strong>&nbsp;</strong></div>
            <p class="locator"><strong>Central Store near you :&nbsp;</strong><a href="https://centralandme.com/store-locator/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://centralandme.com/store-locator/&amp;source=gmail&amp;ust=1568965722784000&amp;usg=AFQjCNGO54evlRWUOF6RRxidABQdd3C7bQ">https://centralandme.com/store-locator/</a></p>
        </div>

    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <script src="js/app.js"></script>
</body>
</html>
