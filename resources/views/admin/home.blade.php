@extends('admin.layouts.master')
@section('content')
    <section class="">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <div class="row clearfix">

                <a href="{{url('/admin/user')}}">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box bg-orange hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">person_add</i>
                            </div>
                            <div class="content">
                                <div class="text">Total User</div>
                                <div class="number count-to" data-from="0" data-to="1225" data-speed="1000"
                                     data-fresh-interval="20">{{$totalUser}}</div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{url('/admin/home')}}">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box bg-pink hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">playlist_add_check</i>
                            </div>
                            <div class="content">
                                <div class="text">Total Coupon</div>
                                <div class="number count-to" data-from="0" data-to="125" data-speed="15"
                                     data-fresh-interval="20">{{$totalPlayedUser}}</div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>
@endsection

