@extends('admin.layouts.master')
<style>
    .searchBar {
        margin-right: 22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            User
                        </h2>
                    </div>
                    <br>
                    <a href="{{url('admin/downloadfile/xlsx')}}" class="btn btn-success btn-sm waves-effect"
                       title="Add New User" style="margin-left: 22px;" aria-disabled="true">
                        <i class="material-icons">cloud_download</i> Download EXCEL
                    </a>
                    <a href="{{url('admin/downloadfile/csv')}}" class="btn btn-info btn-sm waves-effect"
                       title="Add New User" style="margin-left: 22px;" aria-disabled="true">
                        <i class="material-icons">cloud_download</i> Download CSV
                    </a>
                    {!! Form::open(['method' => 'GET', 'url' => '/admin/user', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search..."
                               style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                                 <i class="material-icons" style="height: 27px !important;">search</i>
                             </button>
                              </span>
                    </div>
                    {!! Form::close() !!}
                    <div class="body">
                        <br>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Mobile</th>
                                    <th>Time Taken</th>
                                    <th>Coupon Code</th>
                                    <th>Total Moves</th>
                                    <th>Game Play Date & Time</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>coupon
                                <?php $coupon=null;?>
                                @foreach($user as $key => $item)
                                    <?php

                                    ?>
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->mobile }}</td>
                                        <td>{{ isset($item->coupon->time) ? $item->coupon->time:'--------' }}</td>
                                        <td>{{ isset($item->coupon->couponcode) ?$item->coupon->couponcode:'-------' }}</td>
                                        <td>{{ isset($item->coupon->moves) ?$item->coupon->moves:'-------' }}</td>
                                        <td>{{ isset($item->coupon->created_at) ?date('d F Y h:i A' ,strtotime($item->coupon->created_at)):'-------' }}</td>
                                        <td class="text-center">
                                            <?php
                                            $coupon=App\GameTimer::where('user_id',$item->id)->first();
                                            ?>
                                            @if($coupon)
                                            <a href="{{ url('/admin/user/' . $item->id) }}" title="View User">
                                                <button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i>
                                                    View Coupon Details
                                                </button>
                                            </a>
                                                @else
                                                <span style="color:red">Not-Played</span>
                                                @endif
                                            {{--<a href="{{ url('/admin/user/' . $item->id . '/edit') }}" title="Edit User">--}}
                                                {{--<button class="btn btn-primary btn-xs"><i class="material-icons">mode_edit</i>--}}
                                                    {{--Edit--}}
                                                {{--</button>--}}
                                            {{--</a>--}}

                                            {{--<form method="POST" action="{{ url('/admin/user' . '/' . $item->id) }}"--}}
                                                  {{--accept-charset="UTF-8" style="display:inline">--}}
                                                {{--{{ method_field('DELETE') }}--}}
                                                {{--{{ csrf_field() }}--}}
                                                {{--<button type="submit" class="btn btn-danger btn-xs" title="Delete User"--}}
                                                        {{--onclick="return confirm(&quot;Confirm delete?&quot;)"><i--}}
                                                            {{--class="material-icons">delete</i> Delete--}}
                                                {{--</button>--}}
                                            {{--</form>--}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $user->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
