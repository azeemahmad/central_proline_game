<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\GameTimer;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use App\GeneratedCoupon;
use App\CouponCode;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function savemovestimer(Request $request){
        $data=$request->all();
        $user=Auth::user()->id;
        $check=GameTimer::where('user_id',$user)->first();
        if(!$check){
            $data['user_id']=$user;
            GameTimer::create($data);
            echo 2;
        }
        else{
            echo 1;
        }


    }

    public function displaycoupon(){

        $user=Auth::user();
        $check=GameTimer::where('user_id',$user->id)->where('mailsend','!=',1)->where('coupongenerated','!=',1)->first();
        if($check){
            $minute=explode('mins',$check->time);
            $totalminute=(int)$minute[0];
            $seconds=str_replace(' ','',explode('mins',$minute[1]));
            $sec=explode('secs',$seconds[0]);
            $totalseconds=(int)$sec[0];
            $totalTime=60*$totalminute+$totalseconds;
            if($totalTime < 15){
                $generatedCoupon=GeneratedCoupon::whereNotNull('rs_500')->pluck('rs_500');
                if(count($generatedCoupon)<500){
                $coupon=CouponCode::first();
                if($coupon){
                    $gen=new GeneratedCoupon();
                    $gen->rs_500=$coupon->rs_500;
                    $gen->save();
                    $discout=500;
                    $finalCoupon=$coupon->rs_500;
                }
                // else{
                //     $discout=0;
                //     $finalCoupon='XXXXXXX';
                // }
            }else{
                $discout=0;
                $finalCoupon='XXXXXXX';
            }
            }
            elseif($totalTime >= 15 && $totalTime <= 30){

                $generatedCoupon=GeneratedCoupon::whereNotNull('rs_250')->pluck('rs_250');
                if(count($generatedCoupon)<2000){
                $coupon=CouponCode::first();
                if($coupon){
                    $gen=new GeneratedCoupon();
                    $gen->rs_250=$coupon->rs_250;
                    $gen->save();
                    $discout=250;
                    $finalCoupon=$coupon->rs_250;
                }
                // else{
                //     $discout=0;
                //     $finalCoupon='XXXXXXX';
                // }
            }else{
                $discout=0;
                $finalCoupon='XXXXXXX';
            }
            }
            elseif($totalTime > 30 && $totalTime <= 60){
                $generatedCoupon=GeneratedCoupon::whereNotNull('rs_100')->pluck('rs_100');
                if(count($generatedCoupon)<5000){
                $coupon=CouponCode::first();
                if($coupon){
                    $gen=new GeneratedCoupon();
                    $gen->rs_100=$coupon->rs_100;
                    $gen->save();
                    $discout=100;
                    $finalCoupon=$coupon->rs_100;
                }
                // else{
                //     $discout=0;
                //     $finalCoupon='XXXXXXX';
                // }
            }else{
                $discout=0;
                $finalCoupon='XXXXXXX';
            }
            }
            else{
                $discout=0;
                $finalCoupon='XXXXXXX';
            }
            $coupunid=$finalCoupon;

             if($coupunid != 'XXXXXXX'){
                file_get_contents('http://pt.k9media.in/submitsms.jsp?user=PROLINE&key=f0a8044e0cXX&mobile=+91'.$user->mobile.'&message='.urlencode('Your voucher code is '.$coupunid.'. This voucher is valid upto 31st Oct 2019. For T&C click on http://central.prolineindia.com/termandconditions').'&senderid=PROLNE&accusage=2');
            }
           // $data=['discount'=>$discout,'couponid' => $coupunid,'email' =>$user->email];
           // View::share('data', $data);

            try {
//                Mail::send('emails.coupungeneration', $data, function ($message) use ($data) {
//
//                    $message->from('info@proline.in', 'Proline - Coupon');
//                    $message->to($data['email'])->subject('Proline Coupon');
//                });
                $check->mailsend=1;
                $check->coupongenerated=1;
                $check->couponcode=$coupunid;

            } catch (\Exception $e) {
                $check->mailsend=0;
            }
            $check->save();
            return view('coupon',compact('discout','coupunid'));
        }
        else{
            $discout=0;
            $coupunid='XXXXXXX';
            $alradayplayed=1;
            return view('coupon',compact('discout','coupunid','alradayplayed'));
        }

    }
}
