<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneratedCoupon extends Model
{
    protected $table = 'generated_coupon';
    protected $fillable = [
        'rs_100', 'rs_250', 'rs_500'
    ];

}
