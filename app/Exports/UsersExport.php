<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection , WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $users=User::all();
        $userDetails=array();
        foreach($users as $uk => $uv){
            $userDetails[]=$this->userDetail($uv->id);
        }
        $user=collect($userDetails);
        return $user;
    }


    public function headings(): array
    {
        return [
            'User Mobile',
            'Time Taken',
            'Coupon Code',
            'Total Moves',
            'Game Play Date & Time',
        ];
    }

    public function userDetail($id){
        $item=User::find($id);
        return [
            'User Mobile'=>$item->mobile,
            'Time Taken'=>isset($item->coupon->time) ? $item->coupon->time:'--------',
            'Coupon Code'=>isset($item->coupon->couponcode) ?$item->coupon->couponcode:'-------',
            'Total Moves'=>isset($item->coupon->moves) ?$item->coupon->moves:'------',
            'Game Play Date & Time'=>isset($item->coupon->created_at) ?date('d F Y h:i A' ,strtotime($item->coupon->created_at)):'-------'
        ];
    }
}
